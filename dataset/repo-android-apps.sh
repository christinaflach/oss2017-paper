#!/bin/sh

RELEASES="
  android-1.6_r1.2
  android-1.6_r1.5
  android-2.0_r1
  android-2.1_r2.1p2
  android-2.2_r1
  android-2.2.3_r2
  android-2.3_r1
  android-2.3.7_r1
  android-4.0.1_r1
  android-4.0.4_r2.1
  android-4.1.1_r1
  android-4.3.1_r1
  android-5.1.0_r1
"

APPS="
  Browser
  Calculator
  Calendar
  Camera
  Contacts
  Email
  HTMLViewer
  Mms
  Music
  PackageInstaller
  Settings
  SoundRecorder
  Stk
  VoiceDialer
"

# apt-get install texlive-publishers texlive-lang-portuguese latex-mk

which curl > /dev/null; if [ $? -ne 0 ]; then
  sudo apt-get update
  sudo apt-get install -y curl
fi

which python > /dev/null; if [ $? -ne 0 ]; then
  sudo apt-get install -y python
fi

which git > /dev/null; if [ $? -ne 0 ]; then
  sudo apt-get install -y git
fi

which analizo > /dev/null; if [ $? -ne 0 ]; then
  sudo su -c "echo 'deb http://www.analizo.org/download/ ./' > /etc/apt/sources.list.d/analizo.list"
  curl http://www.analizo.org/download/signing-key.asc | sudo apt-key add -
  sudo apt-get update
  sudo apt-get install -y analizo
fi

if [ ! -x ~/bin/repo ] || [ ! -s ~/bin/repo ]; then
  mkdir -p ~/bin
  curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
  chmod +x ~/bin/repo
fi

PATH=$PATH:~/bin

if [ ! -d WORKING_DIRECTORY ]; then
  mkdir WORKING_DIRECTORY
  cd WORKING_DIRECTORY
  repo init -u https://android.googlesource.com/platform/manifest
else
  cd WORKING_DIRECTORY
fi

for release in $RELEASES; do
  repo init -b $release
  #APPS=$(repo manifest | grep platform\/packages\/apps | sed 's/.*name="\([^"]\+\).*/\1/')
  for app in $APPS; do
    name=$(basename $app)
    if [ ! -e ../$release-$name.metrics ]; then
      repo sync packages/apps/$app
      if [ -d packages/apps/$name ]; then
        analizo metrics packages/apps/$name > ../$release-$name.metrics
      fi
    fi
  done
done
