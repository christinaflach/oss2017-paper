all: rtex bib tex
	pdflatex oss2017-android-metrics.tex

bib: bibliografia.bib
	pdflatex oss2017-android-metrics.tex
	bibtex oss2017-android-metrics

tex: oss2017-android-metrics.tex
	pdflatex oss2017-android-metrics.tex
	pdflatex -output-directory dataset/ dataset/metricas.tex

rtex:
	Rscript oss2017-android-metrics.R

clean:
	rm *.bbl *.aux *.blg *.log *.pdf 
	rm figuras/dist/*.pdf cache/*
	rm oss2017-android-metrics.tex
	rmdir cache

